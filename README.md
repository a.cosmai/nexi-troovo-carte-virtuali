Questo progetto prevede l'implementazione della generazione di carte virtuali con servizio Troovo Nexi per sistemi BT. La generazione delle carte con tale servizio avverrà all'interno di ogni provider di prodotto che prevede l'utilizzo di una carta di credito per la conferma della prenotazione.
Attualmente i connettori sui quali si lavorerà saranno:
1. Bookingcom hotel
1. NTV treni
1. Travelfusion voli

Il **Team** che si occuperà della realizzazione di questo progetto è il seguente:

**PM**: Annalisa Cosmai <br>
**Analista**: Salerno, Mezzina<br>
**Dev**: Salerno, Mezzina, Tarantino<br>
**Tester**: Pellegrini<br>
**QA**: Mezzina


I riferimenti del **cliente**:

**PM**: Taddeo Eugenio <Eugenio.Taddeo@nexi.it><br>
**Ref. tecnico**: Kurt Knackstedt <kurt.knackstedt@troovo.com>
